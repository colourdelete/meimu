from . import config_
config = config_.load()

import sys
from direct.showbase.ShowBase import ShowBase


class app(ShowBase):

    def __init__(self, *args, use_rp=config['use_rp'], **kwargs):
        # Notice that you must not call ShowBase.__init__ (or super), the
        # render pipeline does that for you. If this is unconvenient for you,
        # have a look at the other initialization possibilities.

        # Insert the pipeline path to the system path, this is required to be
        # able to import the pipeline classes. In case you placed the render
        # pipeline in a subfolder of your project, you have to adjust this.
        if use_rp: # use_render_pipeline
            sys.path.insert(0, config['rp_sys_path'])
            from rpcore import RenderPipeline
            self.render_pipeline = RenderPipeline(*args, **kwargs)
            self.render_pipeline.create(self)
        else:
            super().__init__(self, *args, **kwargs)
        self.pointer = self.win.getPointer(0)
    
    def load_model(self, path, render=True, scale=config['model_default_scale'], pos=config['model_default_pos']):
        model = self.loader.loadModel(path) # load model
        model.setScale(*scale) # set scale
        model.setPos(*pos) # set pos
        if render:
            model.reparentTo(self.render) # if rendering, reparent to render
        return model